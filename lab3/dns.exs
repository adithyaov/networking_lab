defmodule Dns do
	def client(port, address, dest_port, lookup_host) do
		case :gen_udp.open(port, [:binary, active: false]) do
		   	{:ok, socket} ->
		   		IO.puts("Client ok socket")
		   		client_send(socket, address, dest_port, lookup_host)
		    {:error, _} ->
		    	IO.puts("Error client")
		end
	end

	def server(port) do
		case :gen_udp.open(port, [:binary, active: false]) do
		   	{:ok, socket} ->
		   		IO.puts("Server ok socket")
		   		server_recieve(socket)
		    {:error, _} ->
		    	IO.puts("Error server")
		end
	end

	def client_send(socket, address, port, lookup_host) do
		header_data = %{
			id: :rand.uniform(255),
			qr: 0,
			opcode: 0,
			aa: 0,
			tc: 0,
			rd: 0,
			ra: 0,
			rcode: 0,
			qdcount: 1,
			ancount: 0,
			nscount: 0,
			arcount: 0	
		}
		question_data = %{
			qname: lookup_host,
			qtype: 1,
			qclass: 1
		}
		case :gen_udp.send(socket, address, port, make_header(header_data) <> make_question(question_data)) do
			:ok ->
				IO.puts("Sent :-)")
		    {:error, _} ->
		    	IO.puts("Error client_send")
		end
		client_recieve(socket)
	end

	def client_recieve(socket) do
		IO.puts("Recieving...")
		case :gen_udp.recv(socket, 512) do
			{:ok, {{a, b, c, d}, port, packet}} ->
				IO.puts("Server: #{a}.#{b}.#{c}.#{d}:#{port}")
				parse_client(packet)
			{:error, _} ->
				IO.puts("Error client_send")
		end
	end

	def server_recieve(socket) do
		case :gen_udp.recv(socket, 512) do
			{:ok, {{a, b, c, d}, port, packet}} ->
				spawn fn ->
					parse_server(socket, {{a, b, c, d}, port, packet})
				end
				server_recieve(socket)
			{:error, _} ->
				IO.puts("Error server_recieve")
		end
	end

	defp parse_header(total_packet) do
		<<
			id :: 16,
			<<
				qr :: 1,
				opcode :: 4,
				aa :: 1,
				tc :: 1,
				rd :: 1,
				ra :: 1,
				_z :: 3,
				rcode :: 4
			>>,
			qdcount :: 16,
			ancount :: 16,
			nscount :: 16,
			arcount :: 16,
			from_question :: binary
		>> = total_packet
		header = %{
			id: id,
			qr: qr, 
			opcode: opcode,
			aa: aa,
			tc: tc,
			rd: rd,
			ra: ra,
			rcode: rcode,
			qdcount: qdcount,
			ancount: ancount,
			nscount: nscount,
			arcount: arcount
		}
		%{
			header: header,
			from_question: from_question
		}
	end

	defp parse_question(from_question) do
		qname_details = parse_qname(from_question, [])
		qname = String.slice(List.foldl(qname_details.qname_list, "", &(&2 <> "." <> &1)), 1..-1)

		<<
			qtype :: 16,
			qclass :: 16,
			from_answer :: binary
		>> = qname_details.rest

		question = %{
			qtype: qtype,
			qclass: qclass,
			qname: qname
		}
		%{
			question: question,
			from_answer: from_answer
		}
	end

	defp parse_answer(from_answer) do
		<< 
			<<3 :: 2>>,
			offset :: 14,
			type :: 16,
			class :: 16,
			ttl :: 32,
			rdlength :: 16,
			a :: 8,
			b :: 8,
			c :: 8,
			d :: 8,
			from_authoity :: binary
		>> = from_answer
		answer = %{
			offset: offset,
			type: type,
			class: class,
			ttl: ttl,
			rdlength: rdlength,
			a: a,
			b: b,
			c: c,
			d: d
		}
		%{
			answer: answer,
			from_authoity: from_authoity
		}
	end

	defp parse_client(packet) do
		header_details = parse_header(packet)
		rcode = header_details.header.rcode
		from_question = header_details.from_question

		if (rcode != 0) do
			IO.puts("Packet error :-(")
			Process.exit(self, :normal)
		end

		question_details = parse_question(from_question)
		qname = question_details.question.qname
		from_answer = question_details.from_answer


		answer_details = parse_answer(from_answer)
		a = answer_details.answer.a
		b = answer_details.answer.b
		c = answer_details.answer.c
		d = answer_details.answer.d

		IO.puts("Name: #{qname}")
		IO.puts("Address: #{a}.#{b}.#{c}.#{d}")
	end

	defp parse_server(socket, {{a, b, c, d}, port, packet}) do
		IO.puts("Request from: #{a}.#{b}.#{c}.#{d}:#{port}")

		header_details = parse_header(packet)
		from_question = header_details.from_question
		question_details = parse_question(from_question)
		qname = question_details.question.qname

		new_question_data = question_details.question

		if qname == "www.james.bond" do
			new_header_data = %{header_details.header | ancount: 1, qr: 1, rcode: 0, aa: 1}
			answer_data = %{
				offset: 12,
				type: 1,
				class: 1,
				ttl: 25,
				rdlength: 4,
				a: 7,
				b: 7,
				c: 7,
				d: 7
			}
		else
			new_header_data = %{header_details.header | ancount: 1, qr: 1, rcode: 3}
			answer_data = %{
				offset: 12,
				type: 2,
				class: 1,
				ttl: 25,
				rdlength: 6,
				a: 11,
				b: 22,
				c: 33,
				d: 44
			}
		end

		response_packet = make_header(new_header_data) <> make_question(new_question_data) <> make_answer(answer_data)
		
		case :gen_udp.send(socket, {a, b, c, d}, port, response_packet) do
			:ok ->
				IO.puts("Response sent to #{a}.#{b}.#{c}.#{d}:#{port}")
		    {:error, _} ->
		    	IO.puts("Error client_send")
		end
		
	end

	defp parse_qname(data, acc) do
		<<
			offset :: 8,
			rest1 :: binary
		>> = data
		offset_in_bits = offset * 8
		if (offset_in_bits == 0) do
			%{
				qname_list: acc,
				rest: rest1
			}
		else
			<<
				stack :: size(offset_in_bits),
				rest2 :: binary
			>> = rest1
			parse_qname(rest2, acc ++ [<<stack::size(offset_in_bits)>>])
		end
	end

	defp make_header(data) do
		# Set header
		id = <<data.id :: 16>>
		qr = <<data.qr :: 1>> # 0 -> req, 1-> res
		opcode = <<data.opcode :: 4>>
		aa = <<data.aa :: 1>>
		tc = <<data.tc :: 1>>
		rd = <<data.rd :: 1>>
		ra = <<data.ra :: 1>>
		z = <<0 :: 3>>
		rcode = <<data.rcode :: 4>>
		qdcount = <<data.qdcount :: 16>>
		ancount = <<data.ancount :: 16>>
		nscount = <<data.nscount :: 16>>
		arcount = <<data.arcount :: 16>>
		(
			id <>
			<<
				qr::bitstring,
				opcode::bitstring,
				aa::bitstring,
				tc::bitstring,
				rd::bitstring,
				ra::bitstring,
				z::bitstring,
				rcode::bitstring 
			>> <>
			qdcount <>
			ancount <>
			nscount <>
			arcount
		)	
	end

	defp make_question(data) do
		qname_parts = String.split(data.qname, ".")
		qname = List.foldr(qname_parts, <<0 :: 8>>, &(<<String.length(&1) :: 8>> <> &1 <> &2))
		qtype = <<data.qtype :: 16>>
		qclass = <<data.qclass :: 16>>
		(
			qname <>
			qtype <>
			qclass
		)
	end

	defp make_answer(data) do
		offset = <<data.offset :: 14>>
		type = <<data.type :: 16>>
		class = <<data.class :: 16>>
		ttl = <<data.ttl :: 32>>
		rdlength = <<data.rdlength :: 16>>
		a = <<data.a :: 8>>
		b = <<data.b :: 8>>
		c = <<data.c :: 8>>
		d = <<data.d :: 8>>
		(
			<<
				<<3 :: 2>>::bitstring,
				offset::bitstring
			>> <>
			type <>
			class <>
			ttl <>
			rdlength <>
			a <>
			b <>
			c <>
			d
		)
	end
end


case System.argv do
  ["client"] ->
	Dns.client(3000, {127, 0, 1, 1}, 53, "google.com")
  ["client", lookup_host] ->
	Dns.client(3000, {127, 0, 1, 1}, 53, lookup_host)
  ["client", lookup_host, port] ->
	Dns.client(String.to_integer(port), {127, 0, 1, 1}, 53, lookup_host)
  ["client", lookup_host, port, server] ->
  	[server_ip, server_port] = String.split(server, ":")
  	[a, b, c, d] = String.split(server_ip, ".")
	Dns.client(
		String.to_integer(port), 
		{String.to_integer(a), String.to_integer(b), String.to_integer(c), String.to_integer(d)},
		String.to_integer(server_port),
		lookup_host
	)

  ["server"] ->
	Dns.server(4000)
  ["server", port] ->
	Dns.server(String.to_integer(port))
end

# Why is response code 0 for domain which does not exist