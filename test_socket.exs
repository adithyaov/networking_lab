defmodule Reciever do
  def accept(port) do
    {:ok, socket} = :gen_tcp.listen(port, [:binary, packet: :line, active: false, reuseaddr: true])
    Logger.info "Accepting connections on port #{port}"
    loop_acceptor(socket)
    # Demonic threaded model => Main process exits == Child Processess exits :-(
    # Loop Main Process to wait
  end

  def loop_acceptor(socket) do
    {:ok, client} = :gen_tcp.accept(socket)
    spawn fn -> # Spawn a different process to serve the client, for concurrency
      serve(client)
    end
    loop_acceptor(socket)
  end

  defp serve(socket) do
    socket
    |> read_line()
    |> write_line(socket)

    serve(socket)
  end

  defp read_line(socket) do
    {:ok, data} = :gen_tcp.recv(socket, 0)
    data
  end

  defp write_line(line, socket) do
    :gen_tcp.send(socket, line)
  end

end

Reciever.accept(1234)