defmodule Echo do
  def accept(port) do
    opts = [:binary, packet: :line, active: false, reuseaddr: true]
    {:ok, socket} = :gen_tcp.listen(port, opts)
    IO.puts("Accepting connections on port #{port}")
    loop_acceptor(socket)
  end

  def loop_acceptor(socket) do
    {:ok, client} = :gen_tcp.accept(socket)
    spawn fn ->
      :gen_tcp.send(client, "You are now connected to the echo server :-)\n")
      serve(client)
    end
    loop_acceptor(socket)
  end

  defp serve(client) do
    case :inet.peername(client) do
      {:ok, {{a, b, c, d}, port}} ->
        recieve(client, a, b, c, d, port)
      {:error, error} ->
        IO.puts("#{error}")
    end
  end

  defp recieve(client, a, b, c, d, port) do
    case :gen_tcp.recv(client, 0) do
      {:ok, data} ->
        if data == :eof do
          IO.puts("Recieved EOF, closed connection at #{a}.#{b}.#{c}.#{d}:#{port}\n")
        end
        IO.puts("#{a}.#{b}.#{c}.#{d}:#{port} -> " <> data)
        :gen_tcp.send(client, data)
        serve(client)
      {:error, error} ->
        IO.puts("Connection closed at #{a}.#{b}.#{c}.#{d}:#{port} with message -> #{error}")
    end
  end

end

case System.argv do
  [] ->
    Echo.accept(1234)
  [arg1] ->
    Echo.accept(String.to_integer(arg1))
end