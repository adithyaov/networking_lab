from hashlib import sha512, sha256, md5
from random import random
from math import log

hash = lambda x: sha512(x).hexdigest()

base = log(16, 2)
hash_len = int(len(hash('Adithya')) * base)


def gen_pair():
	private_key = []
	public_key = []
	for x in range(hash_len):
		secret = (hash(str(random())), hash(str(random())))
		public = (hash(secret[0]), hash(secret[1]))
		private_key.append(secret)
		public_key.append(public)
	return (private_key, public_key)


def sign_message(message):
	msg_hash = hash(message)
	bin_rep = ''.join(['{0:04b}'.format(int(i, 16)) for i in msg_hash])
	(private_key, public_key) = gen_pair()
	signature = []
	for i in range(len((bin_rep))):
		signature.append(private_key[i][int(bin_rep[i])])
	signature_str = ''.join(signature)
	public_key_str = ''.join(['{}{}'.format(x[0], x[1]) for x in public_key])
	return (msg_hash, signature_str, public_key_str)



def verify(message, signature_str, public_key_str):
	msg_hash = hash(message)
	bin_rep = ''.join(['{0:04b}'.format(int(i, 16)) for i in msg_hash])
	signature = []
	public_key = []
	truth = []
	offset = int(hash_len / base)
	for i in range(hash_len):
		start_from_s = offset * i
		start_from_pk = start_from_s * 2
		signature_part = signature_str[start_from_s:(start_from_s+offset)]
		public_key_part = (public_key_str[start_from_pk:(start_from_pk+offset)], public_key_str[(start_from_pk+offset):(start_from_pk+2*offset)])
		truth.append(hash(signature_part) == public_key_part[int(bin_rep[i])])
	return reduce(lambda x, y: x * y, truth, True)


def create_signature(file_path):
	with open(file_path, 'r') as f:
		content = f.read()
		(msg_hash, signature_str, public_key_str) = sign_message(content)
	with open(signature_path, 'w') as f:
		f.write(signature_str)
	with open(public_key_path, 'w') as f:
		f.write(public_key_str)
	print 'Done'

def verify_signature(file_path, signature_path, public_key_path):
	with open(file_path, 'r') as f:
		content = f.read()
	with open(signature_path, 'r') as f:
		signature_str = f.read()
	with open(public_key_path, 'r') as f:
		public_key_str = f.read()
		print bool(verify(content, signature_str, public_key_str))



file_path = './hash/content.data'
signature_path = './hash/signature.data'
public_key_path = './hash/public_key.data'

create_signature(file_path)

verify_signature(file_path, signature_path, public_key_path)


