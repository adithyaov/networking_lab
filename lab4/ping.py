import socket, sys, struct, select, time, random


def checksum(string_to_be_evaluated):
	sum = 0
	string_length = (len(string_to_be_evaluated) / 2) * 2
	count = 0
	while count < string_length:
		this_val = ord(string_to_be_evaluated[count + 1])*256+ord(string_to_be_evaluated[count])
		sum = sum + this_val
		sum = sum & 0xffffffff 
		count = count + 2
	if string_length < len(string_to_be_evaluated):
		sum = sum + ord(string_to_be_evaluated[len(string_to_be_evaluated) - 1])
		sum = sum & 0xffffffff 
	sum = (sum >> 16) + (sum & 0xffff)
	sum = sum + (sum >> 16)
	answer = ~sum
	answer = answer & 0xffff
	answer = answer >> 8 | (answer << 8 & 0xff00)
	return answer


def create_ip_header(
		ip_version=4,
		ip_ihl=5,
		ip_type_of_service=0,
		ip_total_length=0,
		ip_identification=99,
		ip_fragment_offset=0,
		ip_time_to_live=200,
		ip_protocol=1,
		ip_check=0,
		src_ip_addr='10.64.11.52',
		dest_ip_addr='172.217.26.174'
	):
	ip_source_address = socket.inet_aton (src_ip_addr)
	ip_destination_address = socket.inet_aton (dest_ip_addr)
	ip_ihl_version = (ip_version << 4) + ip_ihl
	ip_header = struct.pack('!BBHHHBBH4s4s' , ip_ihl_version, ip_type_of_service, ip_total_length, ip_identification, 
		ip_fragment_offset, ip_time_to_live, ip_protocol, ip_check, ip_source_address, ip_destination_address)
	return ip_header

def create_icmp_packet(
		icmp_packet_id=22,
		icmp_data='hola!',
		icmp_type=8,
		icmp_code=0,
		icmp_seq_num=1
	):
	header = struct.pack('bbHHh', icmp_type, icmp_code, 0, icmp_packet_id, icmp_seq_num)
	chk = checksum(header + icmp_data)
	header = struct.pack('bbHHh', icmp_type, icmp_code, socket.htons(chk), icmp_packet_id, icmp_seq_num)
	return header + icmp_data

def icmp_recieve(packet_id, seq_num, timeout):
	receive_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP)
	start_time = time.time()
	ready = select.select([receive_socket], [], [], timeout)
	if ready[0] == []:
		receive_socket.close()
		return (-1, 'Timed out')
	receive_time = time.time()
	received_packet, received_address = receive_socket.recvfrom(1024)
	(ip, port) = received_address
	icmp_header = received_packet[20:28]
	_, _, _, received_packet_id, recieved_seq_num = struct.unpack('bbHHh', icmp_header)
	receive_socket.close()
	if received_packet_id == packet_id and recieved_seq_num == seq_num:
		return (receive_time - start_time, ip)

def create_ip_packet(ip_header, payload):
	new_ip_header = create_ip_header(ip_check=checksum(ip_header + payload))
	return new_ip_header + payload

def ping(destination_address, number_of_times=5):
	if number_of_times < 0:
		return
	client_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_RAW)
	seq_num = random.randint(0, 1000)
	packet_id = 100
	destination_port = 0
	icmp_packet = create_icmp_packet(icmp_packet_id=packet_id, icmp_seq_num=seq_num)
	
	ip_packet = create_ip_packet(create_ip_header(), icmp_packet)
	
	ping_return = client_socket.sendto(ip_packet, (destination_address, destination_port))		
	time_taken, ping_response = icmp_recieve(packet_id, seq_num, 4)
	if time_taken == -1:
		print ping_response
		exit()
	time_taken = round(time_taken * 1000.0, 4)
	print ping_response + ': ' + str(time_taken) + ' milliseconds.'
	client_socket.close()
	ping(destination_address, number_of_times=number_of_times-1)

try:
	ping_to = sys.argv[1]
except:
	ping_to = 'google.com'

try:
	number_of_times = sys.argv[2]
except:
	number_of_times = 10

ping(ping_to, int(number_of_times))

	