import socket, sys, struct, select, time, random


def checksum(string_to_be_evaluated):
	sum = 0
	string_length = (len(string_to_be_evaluated) / 2) * 2
	count = 0
	while count < string_length:
		this_val = ord(string_to_be_evaluated[count + 1])*256+ord(string_to_be_evaluated[count])
		sum = sum + this_val
		sum = sum & 0xffffffff 
		count = count + 2
	if string_length < len(string_to_be_evaluated):
		sum = sum + ord(string_to_be_evaluated[len(string_to_be_evaluated) - 1])
		sum = sum & 0xffffffff 
	sum = (sum >> 16) + (sum & 0xffff)
	sum = sum + (sum >> 16)
	answer = ~sum
	answer = answer & 0xffff
	answer = answer >> 8 | (answer << 8 & 0xff00)
	return 0

def create_ip_header(
        ip_version=4,
        ip_ihl=5,
        ip_type_of_service=0,
        ip_total_length=0,
        ip_identification=99,
        ip_fragment_offset=0,
        ip_time_to_live=200,
        ip_protocol=17,
        ip_check=0,
        src_ip_addr='127.0.0.1',
        dest_ip_addr='127.0.0.1'
    ):
    ip_source_address = socket.inet_aton(src_ip_addr)
    ip_destination_address = socket.inet_aton(dest_ip_addr)
    ip_ihl_version = (ip_version << 4) + ip_ihl
    ip_header = struct.pack('!BBHHHBBH4s4s' , ip_ihl_version, ip_type_of_service, ip_total_length, ip_identification, 
        ip_fragment_offset, ip_time_to_live, ip_protocol, ip_check, ip_source_address, ip_destination_address)
    return ip_header


def create_udp_packet(
		destination_address='127.0.0.1',
		destination_port=3005,
		source_address='127.0.0.1',
		source_port=3000,
	    message='Pong',
	    protocol=socket.IPPROTO_UDP,
	):
    udp_length = 8 + len(message)

    psuedo_src = socket.inet_aton(source_address)
    pseudo_dest = socket.inet_aton(destination_address)
    pseudo_prot = 17
    pseudo_len = udp_length

    chksum = 0

    udp_packet = struct.pack('!HHHH4s4s4sBBH',
        source_port,
        destination_port,
        udp_length,
        chksum,
        message,
        psuedo_src,
        pseudo_dest,
        0,
        pseudo_prot,
        pseudo_len
    )

    chksum = checksum(udp_packet)

    udp_packet = struct.pack('!HHHH4s',
        source_port,
        destination_port,
        udp_length,
        chksum,
        message
    )

    return udp_packet


def main(source_address='127.0.0.1', recieve_port=3000, send_port=4000):
    recieve_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    send_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_RAW)
    recieve_socket.bind((source_address, recieve_port))
    send_socket.bind((source_address, send_port))
    message, address = recieve_socket.recvfrom(1024)
    (destination_address, destination_port) = address
    udp_packet = create_udp_packet(
    	destination_address,
    	destination_port,
    	source_address,
    	recieve_port
    )
    print(address)
    ip_packet = create_ip_header(dest_ip_addr=destination_address) + udp_packet
    send_socket.sendto(ip_packet, (destination_address, destination_port))
    recieve_socket.close()
    send_socket.close()
    main()

main()