defmodule Netcat do
	def tcp_connect(host, port) do
		opts = [:binary, packet: :line, active: false]
		{:ok, socket} = :gen_tcp.connect(host, port, opts)
    parent = self()
		spawn_link fn ->
      tcp_keep_sending(socket, parent)
    end
    spawn_link fn ->
			tcp_keep_recieving(socket, parent)
		end
    receive do
      {:end_main, msg} ->
        IO.puts(msg)
    end
	end
	def tcp_keep_sending(socket, parent) do
		message = IO.gets ""
		if message != :eof do
		 	:gen_tcp.send(socket, message)
		  tcp_keep_sending(socket, parent)
		else
      send(parent, {:end_main, "connection closed!"})
		end
	end
	def tcp_keep_recieving(socket, parent) do
		case :gen_tcp.recv(socket, 0) do
			{:ok, recieved} ->
				IO.puts(recieved |> String.replace("\r", "") |> String.replace("\n", ""))
				tcp_keep_recieving(socket, parent)
			{:error, _} ->
        send(parent, {:end_main, "server closed!"})
		end
	end

  def udp_connect(port) do
    {:ok, socket} = :gen_udp.open(port)
    parent = self()
    spawn_link fn ->
      udp_keep_recieving(socket, parent)
    end
    receive do
      {:end_main, msg} ->
        IO.puts(msg)
    end
  end
  def udp_keep_recieving(socket, parent) do
    case :gen_udp.recv(socket, 0) do
      {:ok, {address, port, packet}} ->
        IO.puts({address, port})
        IO.puts(packet |> String.replace("\r", "") |> String.replace("\n", ""))
        udp_keep_recieving(socket, parent)
      {:error, _} ->
        send(parent, {:end_main, "server closed!"})
    end
  end
end

# [arg1, arg2] = System.argv
# Netcat.tcp_connect(to_char_list(arg1), String.to_integer(arg2))