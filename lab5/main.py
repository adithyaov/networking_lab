'''
Idea:
given r
find generator matrix for hamming code.
'''

import numpy as np
from itertools import chain, combinations

r = 3
n = (2**r) - 1
k = (2**r) - 1 - r

def powerset(iterable):
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1, len(s)+1))

'''
[I | A] = Dual Hamming
r x n
r x r || r x k

[A.T | I] = Hamming
k x r || k x k
'''

I_par = np.identity(r)

A_par = np.zeros((r, k))

I_gen = np.identity(k)


P = list(powerset(range(r)))
for i in range(r):
	for j in range(k):
		sum_to = np.sum([I_par[i, m] for m in P[r + j]])
		A_par[i, j] = sum_to

par_matrix = np.concatenate([I_par, A_par], axis=1) % 2
gen_matrix = np.concatenate([-np.transpose(A_par), I_gen], axis=1) % 2


'''
gen_matrix = k x n
par_matrix = r x n
vector = 1 x k
'''

hamming = lambda vector: np.matmul(vector, gen_matrix) % 2
dual = lambda vector: np.matmul(vector, par_matrix) % 2
chk_hamming_error = lambda vector: np.matmul(par_matrix, vector) % 2
chk_dual_error = lambda vector: np.matmul(gen_matrix, vector) % 2


'''
[1. 1. 0. 1.]
'''

def error_position(vector):
	if int(np.sum(vector)) == 0:
		return -1
	sets = []
	for i in range(k):
		if vector[i] == 1:
			sets.append(set(P[r:][i]))
	return list(set.intersection(*sets))

def dual_flip_bit_at(index, vector):
	i = 0
	if index == -1:
		return vector
	for x in P:
		if list(x) == list(index):
			vector[i] = (vector[i] + 1) % 2
		i = i + 1
	return vector

def ham_flip_bit_at(index, vector):
	i = 0
	s = -1
	for x in index[::-1]:
		s += (2**i) * x
		i = i + 1

	if int(s) == -1:
		return vector
	vector[int(s)] = (vector[int(s)] + 1) % 2
	return vector

# print hamming([1, 0, 0, 1])
# print chk_hamming_error([0, 1, 1, 1, 0, 0, 1])

# print dual([1, 0, 0])
# print chk_dual_error([1, 0, 0, 1, 1, 0, 1])

# print(dual_flip_bit_at(error_position(chk_dual_error([1, 0, 0, 1, 1, 0, 1])), [1, 0, 0, 1, 1, 0, 1]))

# print(ham_flip_bit_at(chk_hamming_error([0, 1, 1, 1, 0, 0, 1]), [0, 1, 1, 1, 0, 0, 1]))



# x = hamming([0, 0, 0, 0])
# chk = [0, 0, 0, 0, 0, 0, 0]

# ng = np.zeros((k, n))
# print ng
# '''
# 1 - 3
# 4 - 6
# '''
# y = chk_hamming_error(chk)
# z = ham_flip_bit_at(y, chk)
# print x
# print y
# print z

# x = dual([1, 0, 0])
# chk = [1, 0, 0, 0, 1, 0, 1]
# y = chk_dual_error(chk)
# ep = error_position(y)
# print ep
# z = dual_flip_bit_at(ep, chk)
# print x
# print y
# print z








print par_matrix
print gen_matrix
