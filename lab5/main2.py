import numpy as np

r = 3
n = (2**r) - 1
k = (2**r) - 1 - r

par_matrix = np.array([[0 for _ in range(r + 2 - len(bin(i + 1)))] + [int(m) for m in list(bin(i + 1))[2:]] for i in range(n)])

def comp_num(vec):
	n = 0
	for i in range(len(vec)):
		n += (2**i) * vec[i]
	return n

def make_gen():
	bit_indeces = []
	gen_matrix = []
	for x in par_matrix:
		bi = []
		if np.sum(x) == 1:
			continue
		vec = [0] * n
		j = -1
		for i in x:
			j += 1
			if i == 0:
				continue
			n_vec = [0] * r
			n_vec[j] = 1
			bi.append(comp_num(n_vec[::-1]))
			vec[comp_num(n_vec[::-1]) - 1] = 1
		bi.append(comp_num(x[::-1]))
		vec[comp_num(x[::-1]) - 1] = 1
		gen_matrix.append(vec)
		bit_indeces.append(set(bi))
	return (np.array(gen_matrix), bit_indeces)

(gen_matrix, bit_indeces) = make_gen()

print bit_indeces


# print np.matmul(gen_matrix, par_matrix) % 2



vt = np.matmul(par_matrix, [1, 1, 0]) % 2
print vt
v = [0, 1, 1, 1, 1, 0, 0]
chk = np.matmul(v, np.transpose(gen_matrix)) % 2
print chk % 2
print [bit_indeces[i] for i in range(k) if chk[i] == 1]



